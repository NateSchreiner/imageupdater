module gitlab.com/nateschreiner/imageupdater

go 1.15

require (
	github.com/bramvdbogaerde/go-scp v1.1.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d
)
