package img

import (
	"fmt"
	"golang.org/x/image/draw"
	"image"
	"image/png"
	"log"
	"os"
)

const (
	LOGFILE = "log.log"
)

type Size struct {
	width  int
	height int
}

var sizes = map[string]Size{
	"480":     {480, 150},
	"480@2x":  {960, 300},
	"720":     {720, 225},
	"720@2x":  {1440, 450},
	"1024":    {1024, 320},
	"1024@2x": {2048, 640},
	"1600":    {1600, 500},
	"1600@2x": {3200, 1000},
}

func writeLog(filename string, result bool) {
	var prefix string
	if result {
		prefix = "Successfully wrote "
	} else {
		prefix = "Error writing "
	}

	message := prefix + filename + "\n"

	fp, err := os.OpenFile(LOGFILE,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	defer fp.Close()

	if err != nil {
		log.Fatal(err)
	}

	_, err = fp.WriteString(message)
	if err != nil {
		log.Fatal(err)
	}
}

func convertAllSizes(src string) {
	sourceImg, err := os.Open(src)
	if err != nil {
		fmt.Println("OS Error")
		panic(err)
	}

	defer sourceImg.Close()

	img, err := png.Decode(sourceImg)
	if err != nil {
		fmt.Println("ImageDecoding Error")
		panic(err)
	}

	for k := range sizes {
		scaled := scaleImage(k, img)
		success := saveImage(getBaseName(), k, scaled)
		writeLog(getBaseName()+k, success)
	}
}

func getBaseName() string {
	return "resized_image-"
}

func scaleImage(key string, src image.Image) image.Image {
	size := sizes[key]
	return scale(src, image.Rect(0, 0, size.width, size.height), draw.ApproxBiLinear)
}

func scale(src image.Image, rect image.Rectangle, scale draw.Scaler) image.Image {
	dst := image.NewRGBA(rect)
	scale.Scale(dst, rect, src, src.Bounds(), draw.Over, nil)
	return dst
}

func saveImage(base string, key string, img image.Image) bool {
	name := base + key + ".jpg"
	out, err := os.Create("resources/" + name)
	if err != nil {
		fmt.Println(err)
		return false
	}

	defer out.Close()

	err = png.Encode(out, img)
	if err != nil {
		fmt.Println(err)
		return false
	}

	return true
}
