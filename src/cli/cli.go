package cli

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/bramvdbogaerde/go-scp"
	"github.com/bramvdbogaerde/go-scp/auth"
	"golang.org/x/crypto/ssh"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

const (
	LS = "ls "
)

func GetRemoteFilenames(path string) []string {
	cmd := LS + path
	client := connectSSH()

	session, err := client.NewSession()
	if err != nil {
		log.Fatal(err)
	}

	defer session.Close()

	files := getFileNames(*session, cmd)

	return files
}

func ConnectSCP() *scp.Client {
	//clientConfig, _ := auth.PasswordKey("906tech", "god43.M9.DVc", ssh.InsecureIgnoreHostKey())
	clientConfig, _ := auth.PasswordKey("getzs", "Pwd4getzsdev", ssh.InsecureIgnoreHostKey())

	// For other authentication methods see ssh.ClientConfig and ssh.AuthMethod

	// Create a new SCP client
	client := scp.NewClient("157.230.232.72:22", &clientConfig)

	//client := scp.NewClient("72.3.250.122:22", &clientConfig)

	err := client.Connect()
	if err != nil {
		fmt.Println("Couldn't establish a connection to the remote server ", err)
	}

	return &client
}

func connectSSH() *ssh.Client {
	config := ssh.ClientConfig{
		User: "906tech",
		Auth: []ssh.AuthMethod{
			ssh.Password("god43.M9.DVc"),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	client, err := ssh.Dial("tcp", "72.3.250.122:22", &config)
	if err != nil {
		log.Fatal(err)
	}

	defer client.Close()

	return client
}

func getFileNames(session ssh.Session, cmd string) []string {
	var b bytes.Buffer
	session.Stdout = &b

	sStderr, err := session.StderrPipe()
	if err != nil {
		log.Fatal(err)
	}

	go io.Copy(os.Stderr, sStderr)

	err = session.Run(cmd)
	if err != nil {
		log.Fatal(err)
	}

	bytes := bytes.NewBuffer(b.Bytes())

	formatted := strings.Split(bytes.String(), string('\n'))
	return formatted
}

func downloadAllFiles(files []string, client *scp.Client) []*os.File {
	var filesTwo = make([]*os.File, 5)
	var r = make([]*os.File, 5)

	for index, file := range filesTwo {
		fmt.Println(string(index))
		fmt.Println(file)
	}

	for index, file := range files {
		local := "/resources/tmp/image_" + string(index) + ".jpg"
		file := DownloadFile(local, file, client)
		r = append(filesTwo, file)
	}

	return r
}

func DownloadFile(local string, remote string, client *scp.Client) *os.File {
	file, err := os.OpenFile(local,
		os.O_RDWR|os.O_CREATE, 0777)

	defer file.Close()

	if err != nil {
		log.Fatal(err)
	}
	err = client.CopyFromRemote(file, remote)

	if err != nil {
		fmt.Println("Error while copying file ", err)
	}

	return file
}

func GetResourceDirectoryPath() string {
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	pathLen := len(basepath)
	realPathLen := pathLen - 7
	path := basepath[0:realPathLen]
	return path
}

func GetDirFromSource(src string, name string) string {
	reader, err := os.OpenFile(src, os.O_RDONLY, 0777)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, name) {
			fmt.Println(line)
			return name
		}
	}

	return "test"
}
