package main

import (
	"flag"
	"fmt"
	"gitlab.com/nateschreiner/imageupdater/src/cli"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

const (
	BrandBannerDir    = "images/ls_browse/"
	BaseDirUrl        = "/var/www/getzs/trunk/"
	SourceDir         = "skin_responsive/customer/main/"
	BrandHeaderSource = "browse.tpl"
)

var seasons = []string{"summer", "spring", "fall", "winter"}

func main() {
	handleInput()
}

func handleInput() {
	localDir := flag.String("d", "", "Specify directory holding the input images.")
	sourceFile := flag.String("s", "", "Specify source image file name.")
	operation := flag.String("o", "", "Specify the operation you would like.")
	flag.Parse()
	fmt.Println(*operation)

	/*
	   1. Open `localDir`
	   2. For each image in directory, find out where that brands header_images are on remote server
	       1. Download source file for particular operation (header_images) (browse.tpl)
	       2. Iterate over `localDir` files
	           1. Parse out `brandName`
	           2. Find `brandName` in `srcFile`
	           3. return the path to that brandsHeader Images
	           4. return map[`brandName`] `remotePath` for all files in `localDir`
	   3. LS each remoteDirectory in `brandNameMap`
	   4. duplicate `sourceFile` and resize the duplicate according to each file in the previous LS command
	       as it relates to the sizes in `sizes` map
	   5. save each image
	*/

	if *sourceFile != "" {
		// Do something with single file
	} else if *localDir != "" {
		if *operation == "images" {

		}
		/* Iterate over directory
		   1. Check if file is of type image
		       IF YES:
		           1. Parse out the file name, get brand name
		           2. Parse the correct remote source code file and get the path for the brand name above
		           3. get every filename from the path found in the source code above
		           4. Duplicate and Resize the local image file in scope according to static `sizes` array
		               for every different size image found in that remote directory listing.
		           5. Save the duplicated/resized local image file with the same exact name from the remote
		               directory listing correspdnding to the images re-size on localdrive
		           6. Overwrite remote directory with newly created local drive of newly sized images

		       IF NO:
		           1. Continue()
		*/
		localFilenames := getLocalFilenames(*localDir)
		var parsedFilenames = make([]string, 0)
		for _, name := range localFilenames {
			if name != "" {
				brandName := parseFilename(name)
				if brandName != "" {
					parsedFilenames = append(parsedFilenames, brandName)
				}
			}
		}

		brandDirs := mapBrandToRemoteDir(parsedFilenames)
		fmt.Println(brandDirs)
	} else {
		log.Fatal("Unclear input.  No work to do")
	}
}

func mapBrandToRemoteDir(brandNames []string) map[string]string {
	brandDirs := make(map[string]string, 0)
	client := cli.ConnectSCP()

	srcFile := cli.DownloadFile(cli.GetResourceDirectoryPath()+"resources/tmp.txt",
		BaseDirUrl+SourceDir+BrandHeaderSource, client)
	client.Close()
	srcFileName := srcFile.Name()
	for _, brandName := range brandNames {
		dir := cli.GetDirFromSource(srcFileName, brandName)
		if dir != "" {
			brandDirs[brandName] = dir
		}
	}

	return brandDirs
}

/*
Filename Examples:

Birkenstockfall21(1).jpg
Bogs.jpg
DarnToughfall21.jpg
tentree2.jpg
wrangler copy.jpg
SmartwoolFall.jpg
*/
func parseFilename(filename string) string {
	parts := strings.Split(filename, ".")
	if len(parts) > 1 {
		if parts[1] == "jpg" || parts[1] == "png" {
			file := parts[0]
			var newFilename = file
			for _, season := range seasons {
				re := regexp.MustCompile(`(?i)` + season)
				newFilename = re.ReplaceAllString(newFilename, "")
			}

			reg, err := regexp.Compile("[^a-zA-Z]+")

			if err != nil {
				log.Fatal(err)
			}

			newFilename = reg.ReplaceAllString(newFilename, "")

			parts = strings.Split(newFilename, " ")
			if len(parts) > 1 {
				if parts[1] == "copy" {
					newFilename = parts[0]
				} else {
					newFilename = parts[0] + parts[1]
				}
			}
			return newFilename
		}

		return ""
	}

	return filename
}

func getLocalFilenames(path string) []string {
	localFiles := make([]string, 5)
	fileInfos, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	for _, fileInfo := range fileInfos {
		localFiles = append(localFiles, fileInfo.Name())
	}

	return localFiles
}
